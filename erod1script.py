import erdostools as et
import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import math
import scipy.stats as st
import numpy as np
from sklearn.linear_model import LinearRegression
import seaborn as sns
import statsmodels.api as sm

colors = [
    "#1f77b4",  # muted blue
    "#ff7f0e",  # safety orange
    "#2ca02c",  # cooked asparagus green
    "#d62728",  # brick red
    "#9467bd",  # muted purple
    "#8c564b",  # chestnut brown
    "#e377c2",  # raspberry yogurt pink
    "#7f7f7f",  # middle gray
    "#bcbd22",  # curry yellow-green
    "#17becf",  # blue-teal
    "#1f1f1f",  # jet black
    "#8dd3c7",  # subtle green
    "#ffffb3",  # subtle yellow
    "#bebada",  # subtle purple
    "#fb8072",  # subtle red
    "#80b1d3",  # subtle blue
    "#fdb462",  # subtle orange
    "#b3de69",  # subtle green
    "#fccde5",  # subtle pink
    "#d9d9d9",  # very light gray
    "#bc80bd",  # subtle purple
    "#ccebc5",  # subtle green
    "#ffed6f",  # subtle yellow-orange
    "#8dd3c7",  # subtle green-blue
    "#ffffb3",  # subtle yellow-green
    "#bebada",  # subtle purple-blue
    "#fb8072",  # subtle red-orange
]

# Read data
ed1graph = et.read_data_e1()
print("Data read")
# Get degree distribution
degree_distribution = nx.degree_histogram(ed1graph)
# Print top 10 nodes by degree
top10 = sorted(ed1graph.degree, key=lambda x: x[1], reverse=True)[:10]
for node in top10:
    print(node[0], node[1])
# Remove first value as it's 0
degree_distribution = degree_distribution[1:]
# Plot degree distribution
plt.plot(degree_distribution)
plt.xlabel("Degree")
plt.ylabel("Number of authors")
plt.title("Degree distribution of Erdos number 1 graph")
plt.savefig("images/erdos1_degree_distribution.png")
plt.clf()

# Powerlaw fit
## Create log/log values for data points
## Replace zeroes with 0.001 to avoid log(0)
x = np.log10(range(1, len(degree_distribution) + 1))
y = np.array(degree_distribution)
y = np.log10(y + 1)
# Replace -inf with 0
y = np.where(y == -np.inf, 0, y)
## Create a dataframe
df = pd.DataFrame({"degree": x, "count": y})
X = sm.add_constant(df["degree"])
ols_model = sm.OLS(df["count"], X)
est = ols_model.fit()
out = est.conf_int(alpha=0.2, cols=None)

fig, ax = plt.subplots()
df.plot(x="degree", y="count", ax=ax, marker='s', linestyle='None')
y_pred = est.predict(X)
x_pred = df.degree.values
ax.plot(x_pred, y_pred)

pred = est.get_prediction(X).summary_frame()
ax.plot(x_pred, pred["mean_ci_lower"], color="red", linestyle="--")
ax.plot(x_pred, pred["mean_ci_upper"], color="red", linestyle="--")

plt.savefig("images/erdos1_degree_distribution_plaw.png")
plt.clf()


# Now compute distribution with only authors of erdos0
# Get erdos0 data
ed0data = et.read_data_e0()
# Get erdos0 authors
ed0authors = ed0data["Name"].tolist()
# Get erdos0 graph
ed0graph = ed1graph.subgraph(ed0authors)
# Get degree distribution with logaritmic scale
degree_distribution = nx.degree_histogram(ed0graph)
degree_distribution = degree_distribution[1:]
# Plot degree distribution
plt.plot(degree_distribution)
plt.xlabel("Degree")
plt.ylabel("Number of authors")
plt.title("Degree distribution of Erdos number 1 graph with only Erdos number 0 authors")
plt.savefig("images/erdos1_degree_distribution_erdos0.png")
plt.clf()

# Powerlaw fit
## Create log/log values for data points
## Replace zeroes with 0.001 to avoid log(0)
x = np.log10(range(1, len(degree_distribution) + 1))
y = np.array(degree_distribution)
y = np.log10(y + 1)
# Replace -inf with 0
y = np.where(y == -np.inf, 0, y)
## Create a dataframe
df = pd.DataFrame({"degree": x, "count": y})
X = sm.add_constant(df["degree"])
ols_model = sm.OLS(df["count"], X)
est = ols_model.fit()
out = est.conf_int(alpha=0.2, cols=None)

fig, ax = plt.subplots()
df.plot(x="degree", y="count", ax=ax, marker='s', linestyle='None')
y_pred = est.predict(X)
x_pred = df.degree.values
ax.plot(x_pred, y_pred)

pred = est.get_prediction(X).summary_frame()
ax.plot(x_pred, pred["mean_ci_lower"], color="red", linestyle="--")
ax.plot(x_pred, pred["mean_ci_upper"], color="red", linestyle="--")

plt.savefig("images/erdos1_degree_distribution_erdos0_plaw.png")
plt.clf()


# Now we want to display for
# each degree in Erdos0 list, the corresponding average value of degrees in Erdos1. (For instance, for
# each author who has written two papers with Erdos, we check their status in Erdos1 and retrieve the
# total number of their count and take the average). 
# Degrees in erdos0 data are the number of joint papers
total_count_collaborations = {}
# Iterate through rows in erdos0 dataframe
for index, row in ed0data.iterrows():
    e0_author = row["Name"]
    degree = row["Joint Papers"]
    if degree not in total_count_collaborations:
        total_count_collaborations[degree] = []
    # Get degree in erdos1
    e1_degree = ed1graph.degree[e0_author]
    # Add to total count
    total_count_collaborations[degree].append(e1_degree)

# Now we have the total count of collaborations for each degree in erdos0
# Create a scatter plot, fill empty x values with 0 y values
x = []
y = []
# Sort keys
total_count_collaborations = dict(sorted(total_count_collaborations.items()))
for key in total_count_collaborations.keys():
    # Calculate average number of collaborations
    avg_collabs = sum(total_count_collaborations[key]) / len(total_count_collaborations[key])
    y.append(total_count_collaborations[key])
    x.append(key)
# Plot box plot of the data
plt.boxplot(y, labels=x)
plt.title("Number of collaborations in Erdos number 1 graph for each degree in Erdos number 0 graph")
plt.xlabel("Joint papers in Erdos number 0 graph")
plt.ylabel("Number of collaborations in Erdos number 1 graph")
# Save plot to 'images' folder
plt.savefig("images/erdos1_collaborations.png")
plt.clf()

# Remove Erdos
ed1graph_noerdos = et.read_data_e2()
# Draw a high level representation of the graph.
# Change node colors to represent erdos number: 1 = Blue, 2 = Green
# Get erdos number for each node
node_colors = []
node_sizes = []
for node in ed1graph_noerdos.nodes():
    # Get erdos value for node
    erdos_number = ed1graph_noerdos.nodes[node]["Erdos_Number"]
    if erdos_number == 1:
        node_colors.append("blue")
    elif erdos_number == 2:
        node_colors.append("green")
    # Get degree
    node_sizes.append(ed1graph_noerdos.degree[node] * 2)
nx.draw(ed1graph_noerdos, with_labels=False, node_color=node_colors, node_size=node_sizes)
plt.title("Erdos number 1 graph without Erdos")
plt.savefig("images/erdos1_graph_ecolors.png")
# plt.show()
plt.clf()


# Print statistics of above graph:
## Number of nodes
print("Number of nodes: " + str(ed1graph_noerdos.number_of_nodes()))
## Number of edges
print("Number of edges: " + str(ed1graph_noerdos.number_of_edges()))
## Diameter
### Find largest connected component
connected_components = nx.connected_components(ed1graph_noerdos)
# Get list of connected components sorted by size
connected_components = sorted(connected_components, key=len, reverse=True)
largest_connected_component = connected_components[0]
### Size
print("Size of largest component: " + str(len(largest_connected_component)))
print("Percentage of nodes in largest component: " + str(len(largest_connected_component) / ed1graph_noerdos.number_of_nodes()))
print("Size of smallest component: " + str(len(connected_components[-1])))
print("Amount of components in total: " + str(len(connected_components)))
# Draw a high level representation of the graph. Color each component differently
node_colors = []
# Node sizes
node_sizes = []
for node in ed1graph_noerdos.nodes():
    # Check which component the node belongs to
    for i, connected_component in enumerate(connected_components):
        if node in connected_component:
            node_colors.append(colors[i])
            # Add tiny node size as the graph is big
            # If erdos number is 1, triple the size
            if ed1graph_noerdos.nodes[node]["Erdos_Number"] == 1:
                node_sizes.append(2.1)
            else:
                node_sizes.append(0.7)
            break
# nx.draw(ed1graph_noerdos, with_labels=False, node_color=node_colors, node_size=node_sizes)
# plt.show()

largest_connected_component_subgraph = ed1graph_noerdos.copy()
# Remove nodes not in largest connected component, iterating through the components list
for i, connected_component in enumerate(connected_components):
    if i == 0:
        continue
    largest_connected_component_subgraph.remove_nodes_from(connected_component)
## Diameter
print("Approximate diameter of largest component: " + str(nx.approximation.diameter(largest_connected_component_subgraph)))
## Average path length
nodes_numbers, shortest_paths = et.get_nodes_number_and_shortest_paths(largest_connected_component_subgraph)
print("Approximate average path length: " + str(shortest_paths))
## Average clustering coefficient
avg_clustering = nx.average_clustering(largest_connected_component_subgraph)
print("Average clustering coefficient: " + str(avg_clustering))

# Plot degree distribution
degree_distribution = nx.degree_histogram(ed1graph_noerdos)
degree_distribution = degree_distribution[1:]
plt.plot(degree_distribution)
plt.xlabel("Degree")
plt.ylabel("Number of authors")
plt.title("Degree distribution of Erdos number 1 graph without Erdos")
plt.savefig("images/erdos2_degree_distribution_noerdos.png")
plt.clf()

# Powerlaw fit
## Create log/log values for data points
## Replace zeroes with 0.001 to avoid log(0)
x = np.log10(range(1, len(degree_distribution) + 1))
y = np.array(degree_distribution)
y = np.log10(y + 1)
# Replace -inf with 0
y = np.where(y == -np.inf, 0, y)
## Create a dataframe
df = pd.DataFrame({"degree": x, "count": y})
X = sm.add_constant(df["degree"])
ols_model = sm.OLS(df["count"], X)
est = ols_model.fit()
out = est.conf_int(alpha=0.2, cols=None)

fig, ax = plt.subplots()
df.plot(x="degree", y="count", ax=ax, marker='s', linestyle='None')
y_pred = est.predict(X)
x_pred = df.degree.values
ax.plot(x_pred, y_pred)

pred = est.get_prediction(X).summary_frame()
ax.plot(x_pred, pred["mean_ci_lower"], color="red", linestyle="--")
ax.plot(x_pred, pred["mean_ci_upper"], color="red", linestyle="--")

plt.savefig("images/erdos2_degree_distribution_noerdos_plaw.png")
plt.clf()

# Use label propagation algorithm to find communities
# Get communities
communities = nx.algorithms.community.label_propagation.label_propagation_communities(ed1graph_noerdos)
# Create table summarizing the comminities in terms of number of nodes, average path length, diameter and average clustering coefficient.
# Create dataframe
community_data = pd.DataFrame(columns=["Community", "Number of nodes", "Average path length", "Diameter", "Average clustering coefficient"])
# Iterate through communities
for i, community in enumerate(communities):
    subgraph = ed1graph_noerdos.subgraph(community)
    # Get average path length
    average_path_length = nx.average_shortest_path_length(subgraph)
    # Get diameter
    diameter = nx.diameter(subgraph)
    # Get average clustering coefficient
    average_clustering_coefficient = nx.average_clustering(subgraph)
    # Add to dataframe
    community_data.loc[i] = [i, len(community), average_path_length, diameter, average_clustering_coefficient]

# VIsualize the communities as a graph
# Create graph
community_graph = nx.Graph()
# Add nodes
node_sizes = []
for i, community in enumerate(communities):
    community_graph.add_node(i)
    node_sizes.append(len(community) * 2)
# Draw graph
nx.draw(community_graph, with_labels=False, node_size=node_sizes)
plt.title("Communities in Erdos number 1 graph")
plt.savefig("images/erdos1_communities.png")
plt.clf()
# Print entire dataframe
print(community_data)
# Sort by diameter, print largest 10 rows
print(community_data.sort_values(by=["Diameter"], ascending=False).head(10))
# Save to csv
community_data.to_csv("erdos1_communities.csv")