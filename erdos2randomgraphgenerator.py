import erdostools as et
import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import math
import scipy.stats as st
import numpy as np

ed1graph_noerdos = et.read_data_e2()
degree_distribution = nx.degree_histogram(ed1graph_noerdos)
degree_distribution = degree_distribution[1:]
# Use random graph generator (configuration model) to generate a random graph with the same number of nodes and edges as the ed1graph_noerdos and the same degree distribution.
# Repeat the process 100 times, and for each time, calculate the *average* erdos number of *connected* nodes.
# Ensure that sum of degree distribution is even
if sum(degree_distribution) % 2 != 0:
    degree_distribution[-1] += 1
# Create a degree sequence, with length equal to the number of nodes in ed1graph_noerdos
degree_sequence = []
for i, degree in enumerate(degree_distribution):
    degree_sequence.extend([i] * degree)
avg_e_numbers = []
no_connected_components = []
for ind in range(400):
    # Shuffle degree sequence with random permutation
    degree_sequence = np.random.permutation(degree_sequence)
    # Generate random graph, with same degree distribution as ed1graph_noerdos and same total number of nodes
    seed = np.random.randint(0, 1000000)
    random_graph = nx.configuration_model(degree_sequence, seed=seed)
    # Remove parallel edges
    random_graph = nx.Graph(random_graph)
    # Print size of random graph
    for i, node in enumerate(ed1graph_noerdos.nodes()):
        en = ed1graph_noerdos.nodes[node]["Erdos_Number"]
        # Add erdos number to random graph
        random_graph.nodes[i]["Erdos_Number"] = en
    # Remove self loops
    random_graph.remove_edges_from(nx.selfloop_edges(random_graph))
    # Get connected components
    connected_components = nx.connected_components(random_graph)
    # Print number of connected components
    connected_components = sorted(connected_components, key=len, reverse=True)
    connected_components = [cc for cc in connected_components if len(cc) > 1]
    erdos_numbers = []
    for component in connected_components:
        for node in component:
            node = random_graph.nodes[node]
            if "Erdos_Number" not in node:
                continue
            else:
                erdos_number = node["Erdos_Number"]
                erdos_numbers.append(erdos_number)
    # Get average erdos number
    average_erdos_number = sum(erdos_numbers) / len(erdos_numbers)
    avg_e_numbers.append(average_erdos_number)
    no_connected_components.append(len(connected_components))
    # print progress every 10 iterations
    if ind % 40 == 0:
        print(ind)
# Plot histogram of average erdos numbers and fit a gaussian curve to it. Plot the curve on top of the histogram.
# Plot histogram of average erdos numbers
plt.hist(avg_e_numbers, bins=40, density=True)
# Fit gaussian curve to histogram
mu, sigma = st.norm.fit(avg_e_numbers)
x = np.linspace(min(avg_e_numbers), max(avg_e_numbers), 400)
p = st.norm.pdf(x, mu, sigma)
plt.plot(x, p, 'k', linewidth=2)
plt.title("Average Erdos number of connected nodes in random graph with same degree distribution as ed1graph_noerdos")
plt.xlabel("Average Erdos number")
plt.ylabel("Density")
plt.savefig("images/average_erdos_number.png")
plt.clf()

# Plot histogram of number of connected components
plt.hist(no_connected_components, bins=40, density=True)
# Fit gaussian curve to histogram
mu, sigma = st.norm.fit(no_connected_components)
x = np.linspace(min(no_connected_components), max(no_connected_components), 400)
p = st.norm.pdf(x, mu, sigma)
plt.plot(x, p, 'k', linewidth=2)
plt.title("Number of connected components in random graph with same degree distribution as ed1graph_noerdos")
plt.xlabel("Number of connected components")
plt.ylabel("Density")
plt.savefig("images/number_of_connected_components.png")
plt.clf()