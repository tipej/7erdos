# Script for displaying graph, showing the yearly evolution of number of articles published by all the authors in the Erdos number 0 group

# Importing libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
import math
import random

# Reading the data
def read_data_e0(maxNameLength=39):
    erdos_group = []
    file = open("erdos0.txt", "r")
    data = file.readlines()
    file.close()
    # Read data from string
    for line in data:
        name = line[:maxNameLength].strip()
        year = line[maxNameLength:].strip()
        joint_papers = 1
        if ":" in year:
            year, joint_papers = year.split(":")
            joint_papers = int(joint_papers)
        year = int(year)
        erdos_group.append([name, year, joint_papers])
    # Convert to dataframe
    erdos_group = pd.DataFrame(erdos_group, columns=["Name", "Year", "Joint Papers"])
    return erdos_group

def read_data_e1(maxNameLength=39):
    # Get e0 data
    erdos0data = read_data_e0(maxNameLength).set_index("Name")
    # Initialize networkx graph
    erdos_group = nx.Graph()
    # Add Erdos to graph
    # erdos_group.add_node("Erdos", Year=1913, Joint_Papers=0, Erdos_Number=0)
    file = open("erdos1.txt", "r")
    data = file.readlines()
    file.close()
    # Read data from string
    latestAuthor = None
    for line in data:
        # If there are no leading spaces/tabs, then we have a new author
        if line != "\n":
            if line[0] != " " and line[0] != "\t":
                name = line[:maxNameLength].strip()
                year = line[maxNameLength:].strip()
                joint_papers = 1
                if ":" in year:
                    year, joint_papers = year.split(":")
                    joint_papers = int(joint_papers)
                year = int(year)
                # Add author to graph
                erdos_group.add_node(name, Year=year, Joint_Papers=joint_papers, Erdos_Number=1)
                # Add edge between author and Erdos
                # erdos_group.add_edge("Erdos", name)
                latestAuthor = name
            else: # We have collaborating author
                name = line.strip()
                # Add collaborator to graph
                # Check if collaborator already exists as a node
                if name not in erdos_group.nodes:
                    # Check if part of e0 group
                    if name in erdos0data.index:
                        # Get data from e0
                        year = erdos0data.loc[name]["Year"]
                        joint_papers = erdos0data.loc[name]["Joint Papers"]
                        erdos_group.add_node(name, Year=year, Joint_Papers=joint_papers, Erdos_Number=1)
                    else: 
                        erdos_group.add_node(name, Year=0, Joint_Papers=0, Erdos_Number=2)
                # Add edge between author and collaborator, if no edge exists
                if not erdos_group.has_edge(latestAuthor, name):
                    erdos_group.add_edge(latestAuthor, name)
    return erdos_group

def read_data_e2():
    erdos_group = nx.Graph()
    file = open("erdos2.txt", "r")
    data = file.readlines()
    file.close()
    # Read data from string
    latestAuthor = None
    for line in data:
        # If there are no leading spaces/tabs, then we have a new author with e2
        if line != "\n":
            if line[0] != " " and line[0] != "\t":
                name = line.strip()
                erdos_group.add_node(name, Erdos_Number=2)
                latestAuthor = name
            else: # We have collaborating author with e1
                name = line.strip()
                # Add collaborator to graph
                # Check if collaborator already exists as a node
                if name not in erdos_group.nodes:
                    erdos_group.add_node(name, Erdos_Number=1)
                # Add edge between author and collaborator, if no edge exists
                # if not erdos_group.has_edge(latestAuthor, name):
                erdos_group.add_edge(latestAuthor, name)
    return erdos_group

# From https://stackoverflow.com/questions/67496060/networkx-average-shortest-path-length-and-diameter-is-taking-forever
def get_nodes_number_and_shortest_paths(component_, n_samples=1000):
    nodes = component_.nodes()
    lengths = []
    for _ in range(n_samples):
        n1, n2 = random.choices(list(nodes), k=2)
        length = nx.shortest_path_length(component_, source=n1, target=n2)
        lengths.append(length)
    return len(nodes), np.mean(lengths)