import erdostools as et
import pandas as pd
import matplotlib.pyplot as plt

# Method for displaying the graph of the yearly evolution of number of articles published by all the authors in the Erdos number 0 group
def display_graph(erdos_group):
    # Group by year
    erdos_group = erdos_group.groupby("Year").sum()
    # Plot graph
    plt.plot(erdos_group.index, erdos_group["Joint Papers"])
    plt.xlabel("Year")
    plt.ylabel("Number of articles")
    plt.title("Yearly evolution of number of articles published by all the authors in the Erdos number 0 group")
    # Vertical line to mark the year of Erdos's death
    plt.axvline(x=1996, color="red")
    plt.show()

def display_cumulative_graph(erdos_group):
    # Group by year
    erdos_group = erdos_group.groupby("Year").sum()
    # Calculate cumulative sum
    erdos_group["Cumulative Joint Papers"] = erdos_group["Joint Papers"].cumsum()
    # Plot graph
    plt.plot(erdos_group.index, erdos_group["Cumulative Joint Papers"])
    plt.xlabel("Year")
    plt.ylabel("Cumulative number of articles")
    plt.title("Cumulative number of articles published by all the authors in the Erdos number 0 group")
    # Vertical line to mark the year of Erdos's death
    plt.axvline(x=1996, color="red")
    # Add text to mark Erdos's death
    plt.text(1996.5, 1000, "Erdos's death", rotation=0, color="red")
    plt.savefig("images/cumulative_number_of_articles.png")
    plt.clf()
    # plt.show()

print("Reading data...")
erdos_group = et.read_data_e0()
print("Data read")
# Display graph
display_graph(erdos_group)
display_cumulative_graph(erdos_group)